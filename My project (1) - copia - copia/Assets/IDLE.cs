using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDLE : StateMachineBehaviour
{

    [SerializeField]
    private float timeUntilBored;

    [SerializeField]
    private int numberAnimations;

    private bool bored;
    private float waitingTime;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ResetIdle(animator);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!bored)
        {
            waitingTime += Time.deltaTime;

            if (waitingTime > timeUntilBored)
            {
                bored = true;

                int boredAnimation = Random.Range(1, numberAnimations + 1);

                animator.SetFloat("Idle", boredAnimation);
            }
        }
        else if (stateInfo.normalizedTime % 1 > 0.98)
        {
            ResetIdle(animator);
        }
    }

    private void ResetIdle(Animator animator)
    {
        bored = false;
        waitingTime = 0;

        animator.SetFloat("Idle", 0);
    }
}